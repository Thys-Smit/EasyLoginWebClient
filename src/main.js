import Vue from 'vue';
import Vuex from 'vuex';
import App from './App';
import router from './router';
import Axios from 'axios'
import hours from './hours.json' //Used for offline testing
import periods from './periods.json' //Used for offline testing
import dateFormat from 'dateformat'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        users: [],
        selectedUser: '',
        selectedUserID: 0,
        periods: [],
        userHours: {},
        userHoursSummary: {},
        signInState: 0,
        refreshNeeded:0,
    },
    
    mutations: {
        getUsers (state, payload) {
            state.users = payload
        },
        selectUser (state, payload){
            for (var i = 0; i < state.users.length; i++){
                if(state.users[i].IDEmployee === payload){
                    state.selectedUser = state.users[i].Name + ' ' + state.users[i].Surname
                    state.selectedUserID = payload
                    break
                }
            }
        },
        getUserHours (state, payload) {
            // state.userHours = hours //Used for offline testing

            //Get the user hours 
            Axios({
                url: `http://localhost:2022/api/reports/hoursSpecWeek/${payload.id}&${payload.date}`,
                method: 'GET'
            }).then((response) => {
                if(response.data.recordset.length !== 0){
                    state.userHours = response.data
                } else {
                    state.userHours = {}
                }

                //Get 3 week user hour summary
                Axios({
                    url: `http://localhost:2022/api/reports/Summary_specUser3Week/${payload.id}&${dateFormat(payload.date, `yyyy-mm-dd'T'hh:MM:ss.ss'Z'`)}`,
                    method: 'GET'
                }).then((response) => {
                    if(response.data.recordset.length !== 0){
                        state.userHoursSummary = response.data
                    } else {
                        state.userHoursSummary = {}
                    }
                }).catch((error) => {
                    console.log(error);
                });
                
            }).catch((error) => {
                console.log(error);
            });
        },
        punchUserCard (state, payload) {

            var date = dateFormat(payload.date, `yyyy-mm-dd'T'hh:MM:ss.ss'Z'`);

            // Log user In/Out
            Axios({
                url: `http://localhost:2022/api/employee/loginToggle/${payload.id}&${date}`,
                method: 'POST'
            }).then((response) => {

                state.signInState = response.data.result.recordsets[0][0].SignInState

                //Refresh User List in navigation component
                Axios({
                    url: `http://localhost:2022/api/employee/list/${dateFormat(new Date(), `yyyy-mm-dd'T'hh:MM:ss.ss'Z'`)}`,
                    method: 'GET'
                }).then((response) => {
                    state.users = response.data.recordset
                }).catch((error) => {
                    console.log(error)
                });

            }).catch((error) => {
                console.log(error);
            });
        }
    },
    //TODO: Will action work better for the getUserHours mutation?
    actions: {

    }
});

const bus = new Vue();
Object.defineProperty(Vue.prototype, '$bus', { get() { return this.$root.bus } });

new Vue ({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>', 
    data: {
        bus
    }
});