import Vue from 'vue';
import VueResource from 'vue-resource';
import Router from 'vue-router';
import Vuetify from 'vuetify';
import LoginComponent from '@/components/LoginComponent';
import HomeComponent from '@/components/HomeComponent';
import UserHoursComponent from '@/components/UserHoursComponent'
import Toasted from 'vue-toasted';

Vue.use(VueResource);
Vue.use(Router);
Vue.use(Vuetify);
Vue.use(Toasted);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'LoginComponent', 
      component: LoginComponent
    }, 
    {
      path: '/home',
      name: 'HomeComponent',
      component: HomeComponent,
      children: [
        {
          path: '/user/:id', 
          name: 'UserHoursComponent', 
          component: UserHoursComponent
        }
      ]
    }
    
  ]
}); 